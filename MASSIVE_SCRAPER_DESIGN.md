**Overall system Design:**

![System design](http://i.imgur.com/QndeI6S.jpg)

**Explanation:**

1. User adds a root url to scrape
2. Urls are saved in a json file, then every so often a cron jobs picks them up 
3. Urls are put on a queue, for example can be a Celery queue handled by RabbitMQ (Had a prototype but need to look for the code)
4. A Scrapy instance loads the page, extract all the links 
5. The urls are divided into batches, so the process can be parallelised
6. Scraped info with the URL
7. Scraped info are sent to an external entity recognition service - can be Mashape, [for example](https://bitbucket.org/andrea-f/tv-news-videos/src/1e700f68a4cd905d66162d7bcb03ad3f8e4daed2/newsnow_scraper/newsnow_scraper/pipelines.py?at=master&fileviewer=file-view-default#pipelines.py-69 )
 - 7.1 This can be external or custom built with NLTK for instance 
8. The information scraped is augmented with extra data 
9. Scrapy then sends this information to MongoDB to save it in a collection
 - 9.1 This can be Robomongo, or a custom script to pull all the information from the db and see queue status.
10. A cron job that runs a map reduce on the raw collection in the db [https://bitbucket.org/andrea-f/hmwdyka2/src/91af0498daa7accb3206a6289bb93b55a76f2eb7/api_server/js/lib/db.js?at=master&fileviewer=file-view-default](https://bitbucket.org/andrea-f/hmwdyka2/src/91af0498daa7accb3206a6289bb93b55a76f2eb7/api_server/js/lib/db.js?at=master&fileviewer=file-view-default)
11. Create most used categories, media types and people
12. Save back this information in MongoDB in a different collection
13. User can then search through these categories by seeing the most used entities, or do a specific search in all/each collection (similar to [Screenshots section](https://bitbucket.org/andrea-f/hmwdyka2)