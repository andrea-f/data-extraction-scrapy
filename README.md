### What is it for ###
The Data extractor uses Python3 and a Scrapy engine (with Splash and LUA) and a template system to scrape certain parts of websites based on a list of URLS.
The scraped information is saved in a JSON file.

### Requirements ###
* Virtualenv wrapper
* Python3
* Docker

### How to use it ###
* Create a virtualenv and activate it.
* Install requirements: `pip install -r requirements.txt`
* To enable Splash(javascript render): `docker pull scrapinghub/splash`, then `docker run -d -p 8050:8050 scrapinghub/splash --max-timeout 3600`
* The defaults urls to scrape are in `/templates/input_urls.txt` and the scraper config is in `/templates/generic.json`
* To run the scraper run in root directory of project: `python3 extract_data.py --config=[LOCATION_OF_JSON_CONFIG_FILE] --urls=[LOCATION_OF_INPUT_URLS_FILE]`.
* If no options are put in, defaults are used.
* Once the scraper has finished, the output file generated is: `output_scraped_articles.json`, in the root directory of the project.

### Testing ###
* Tests are in `/tests/` folder and can be run by: `python3 -m pytest DataExtractorTest.py -vv`

### Development ###
The tricky bit is the lazy loading images in certain AJAX based sites (such as the BBC's). Images load after a small timeout when the user has reached the scroll height of the image, meaning that with a simple request not all page information will be fetched.
To account for javascript client side rendering, Splash - a Scrapy plugin - is used as a daemon in a Docker container, to handle the actual page rendering. The page can be controlled via LUA, which allows to run javascript in context of the page. Due to the multiple hops, the delays in fetching data are unpredictable. To mitigate this, the tradeoff is that image quality are not the best, to avoid waiting too long for a page to be scraped.

### Release versions ###

** v1.0_static_pages: static page scraping **

** v1.5_js_pages: With JS page scraping: **

*  Added JS rendering with Splash. Runs in a Docker container as a service, used to render a page JS and interact with the page.
* This is necessary for sites which use lazy loading and only load additional information when a user does a certain action (i.e. scroll page).
* Using Splash within a Docker container on a laptop is a tradeoff as the DNS resolution is slow and 504 errors are frequent. Deploying on a VM, for example AWS would certainly speed things up.
* A better way of just setting random timings could be implemented, but having a scalable solution for different sites means that no hardcoded vars can be spied upon in the HTML page to determine if page has fully loaded.