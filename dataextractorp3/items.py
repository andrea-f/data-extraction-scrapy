# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class ImageItem(scrapy.Item):
    url     = scrapy.Field()
    caption = scrapy.Field()
    pass

#class VideoItem(scrapy.Item):
#    url = scrapy.Field()

class Dataextractorp3Item(scrapy.Item):
    headline  = scrapy.Field()
    images = scrapy.Field(serializer=ImageItem)
    url    = scrapy.Field()
    #videos = scrapy.Field(serializer=VideoItem)
    pass
