# -*- coding: utf-8 -*-


from difflib import Differ
import pprint
from dataextractorp3.items import Dataextractorp3Item, ImageItem
from typing import List, Mapping
import json

import os,sys
from scrapy.exporters import JsonItemExporter

class ExtractImagePipeline(object):



    def process_item(self, item, spider):
        images = item['images']
        c = 0
        for c in range(len(images)):
            resolutions = images[c]["resolution_tags"]
            resolutions_clean = []
            for res in resolutions:
                res_split = res.split()
                url = [r for r in res_split if len(str(r)) > 4][0]
                if not url in resolutions_clean:
                    resolutions_clean.append(url)
            resolutions = resolutions_clean
            print("resolutions_clean: %s"%resolutions_clean)
            if len(resolutions) > 1:
                best_resolution = self._select_best_resolution(resolutions)
                # Update the best image in case
                # there is a better version
                images[c]["url"] = [best_resolution]
            # Unpack url list
            elif len(images[c]['url']) == 0 and len(resolutions) == 1:
                images[c]['url'] = resolutions
            images[c]['url'] = images[c]['url'][0]
        image_items = []
        for image in images:
            # Filter caption list for only one,
            # the longest/most informative string
            image['caption'] = max([(len(caption),caption) for caption in image['caption']], key=lambda x:x[0])[1].strip()
            print(image['caption'])
            # Don't need it for requirements
            image.pop('resolution_tags')
            # Create a scrapy item
            #print(ImageItem(**image))
            image_item = ImageItem(**image)
            image_items.append(image_item)
        item['images'] = image_items# {img for img in image_items}
        #print("@@@@@@@@@@@@@@@@ hedaline: %s" % item["headline"])
        
        return item

    def _select_best_resolution(self, resolutions: List[str]) -> str:
        """
        Selects best resolution from available extracted images.
        """
        resolutions = [url for url in resolutions if len(url) > 5]
        pprint.pprint(resolutions)
        best_resolution_url = self.__order_by_file_name(resolutions)

        print("@@@@@@@Best res: %s" % best_resolution_url )
        return best_resolution_url

    def __order_by_file_name(self, resolution_urls: List[str]) -> str:
        """
        Uses a regex to determine the highest resolution.
        """
        # Remove anything which doesnt look like a url or even a file location, by length.
        

        def recursive_diff(url1, url2, diff=[], index=0) -> list:
            """
            Recursively find the difference between two strings.
            """
            if index == 0:
                # Check mid of string to be faster
                # since usually the beginning will be an hostname and equal in all
                index = int(len(url1)/2)
            if index < len(url1):
                if url1[:index] in url2:
                    # Continue halving the strings
                    recursive_diff(url1[index:], url2[index:], diff, int(len(url1)/2))
                else:
                    diffs = []
                    for x in range(len(url1)):
                        # Just in case the second url is shorter
                        try:
                            if not url1[x] in url2[x]:
                                diffs.append(url1[x])
                        except:
                            pass
                    # Join the different characters found
                    joined_diffs = ''.join(diffs)
                    try:
                        # Are the different characters integers?
                        joined_diffs = int(joined_diffs)
                    except:
                        pass
                    diff.append(joined_diffs)
            
            return list(set(diff))
        
        # Cycle through all items
        
        for url in resolution_urls:
            for url2 in resolution_urls:
                diff = recursive_diff(url, url2)
        # Is there a maxium identified?
        max_res = max(diff)
        max_url = ""
        for url in resolution_urls:
            if str(max_res) in url:
                max_url = url
                break
        return max_url



    def __order_by_content_length(self, available_resolutions: List[str]) -> str:
        """
        Makes an http request for header information and checkes the content length size.
        """


class JsonWriterPipeline(object):
    def open_spider(self, spider):
        #now = time.strftime("%c").replace(' ', '_')
        self.file_name = os.getcwd()+'/output_scraped_articles.json'

    def _convert_to_python_dict(self,  **kwargs):
        return 

    def serialize_item(self, entry) -> dict:
        """ Serialises scrapy item to Python dict so it can be saved as JSON."""
        new_entry = {}
        for key, value in entry.items():
            if isinstance(value, list):
                value = [dict(sub_item) for sub_item in value]
            new_entry[key] = value
        return new_entry        

    def process_item(self, entry, spider, over_write=False, file_name=""):
        """ 
        Writes json file out.
        
        :param file_name:
            What is the file name to read/create, string.
        
        :param entry:
            Data to save, dict.
        
        :param over_write:
            Whether to add to the file or create it fresh, defaults to False, boolean.
        
        """
        if len(file_name) == 0:
            file_name = self.file_name

        entry = self.serialize_item(entry)
        if not over_write:
            try:
                f = open(file_name, 'r')
                data = json.load(f)
                # Create an index of urls 
                # So no entry with duplicate url can be inserted
                # Can be improved with a db for large amounts of data
                urls = [item['url'] for item in data]
                f.close()
                try:
                    # Could add an hash to avoid duplicates
                    if not entry['url'] in urls:
                        data.append(entry)
                except:
                    data = [entry]
            except Exception as e:
                data = [entry]
                pass
        else:
            data = [entry]
        f = open(file_name, 'w+')
        json_items = json.dumps(data, indent=4, ensure_ascii=False)

        f.write(json_items)
        f.close()
        print("Wrote %s lines in %s" %(len(data), file_name) )
        return entry