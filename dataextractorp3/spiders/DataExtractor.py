# -*- coding: utf-8 -*-
import os
import sys
import json
import scrapy
from dataextractorp3.spiders.exceptions import (
    HTMLParseError,
    FileReadError
)
from dataextractorp3.items import Dataextractorp3Item, ImageItem
from scrapy.selector import Selector
from scrapy.http import Request
from dataextractorp3 import items
import pprint
from typing import List, Mapping



class Dataextractorp3ItemSpider(scrapy.Spider):

    name = "dataextractorp3"
    custom_settings = {'REDIRECT_MAX_TIMES': 10}



    def __init__(self, config='', urls="", *args, **kwargs):
        """
        Initialise class variables and create workers.
        """
        # Holds config information
        self.config = self.read_config_file(config)
        self.input_urls = self.read_config_file(urls)

    def read_config_file(self, file_name) -> dict:
        """
        Reads json file with dbpedia query information

        :param file_name:
            Location of JSON input file or a file like object with input urls
            with specified parameters to run scraper, string.
        Returns read data.
        """
        
        if len(file_name) == 0:
            return
        if not os.path.isfile(file_name):
            file_name = "%s%s" % (os.getcwd(), file_name)
        try:
            f = open(file_name, "r")
            if file_name.endswith('.json'):
                data = json.load(f)
            else:
                data = {
                    "urls": [u.strip() for u in f.readlines()]
                }
            f.close()
            return data
        except:
            raise FileReadError("[getConfigFile] Error in reading file: %s, quitting." % file_name)

    def lua_script(self) -> str:
        return """
        function main(splash)
          local url = splash.args.url
          assert(splash:go(url))
          assert(splash:runjs("window.scrollTo(0,document.body.scrollHeight/2)"))
          assert(splash:wait(2))
          assert(splash:runjs("window.scrollTo(document.body.scrollHeight/2,document.body.scrollHeight)"))
          assert(splash:wait(3))
          return {
            html = splash:html(),
          }
        end
        """

    def start_requests(self):
        """
        First function to be called by scrapy!
        Built in scrapy function necessary in subclass of scrapy.Spider()
        """    

        # We need a LUA script to tell Splash 
        # To scroll at the bottom of the page
        # And then wait 10 seconds before outputting the page.
        script = self.lua_script()
        # Just cycle through the elements
        # and for each url get it
        input_file = self.input_urls
        for url in input_file['urls']:
            yield Request(
                url,
                callback=self.parse,
                method="GET",
                meta={
                    'splash': {
                        'endpoint': 'execute',#'render.html', #
                        'args': {
                            'lua_source': script,
                            #'width': 1000,
                            #'js_source':"window.scrollTo(0,document.body.scrollHeight);",
                            'wait':0.5,
                            'timeout':60,
                            
                        }
                    }
                }
            )

    def parse(self, response):
        """
        Parses repsonse from Scrapy scraping.
        Can be easily modified to take into account more data to get suche as video.
        Which contains:
        - headline: title of an article,
        - images: list of images in the page. 
          For each image there should be a caption and the highest resolution available url.
        - url: the url of the article

        :param response:
            Contains HTTP response to request, class.
        """
        #Extract images information for each page here:
        item = {
            "url"   : response.url,
            "images": []
        }
        hxs = Selector(response=response)
        # Extract title of article from response page
        try:
            item["headline"] = max(self.extract_information_by_media_type(hxs, self.config['headline']))
        except:
            pass
        # Cycle through image props
        images = []
        # Extract all the image blocks
        images_container = self.extract_information_by_media_type(
            hxs, 
            self.config['image']['container'], 
            extract=False
        )
        # Extract information from the image blocks
        for image in images_container:
            image_data = {}
            for key, rule in self.config['image']['data'].items():
                image_data[key] = self.extract_information_by_media_type(image, rule)
            images.append(image_data)
        item['images'] = images
        entry = Dataextractorp3Item(**item)
        # Send to next step in pipeline
        yield entry
        
    def extract_information_by_media_type(self, hxs, media_type_selector: List[str], extract=True) -> list:
        """ 
        Extracts information from the HTML page based on the rules passed in. 
        """
        data_container = []
        # Different ways to get the same data, multiple classes for instance or tags.
        for selector in media_type_selector:
            try:
                if extract:
                    data_container += hxs.xpath(selector).extract()
                else:
                    data_container += hxs.xpath(selector)
            except Exception as e:
                raise HTMLParseError("Error in parsing HTML data with selector: %s" % media_type_selector)
        # Make sure there are no duplicates
        return list(set(data_container))


                





