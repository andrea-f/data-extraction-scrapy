class FileReadError(Exception):
    """
    [ERROR] The requested resource does not exist.
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

class HTMLParseError(Exception):
    """
    [ERROR] There was an error in parsing the HTML content.
    """
    def __init__(self, error):
        self.error = error

    def __str__(self):
        return repr(self.error)
    pass

