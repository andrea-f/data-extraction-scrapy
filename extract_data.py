from dataextractorp3.spiders import DataExtractor
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import argparse
import os
import sys

def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-c', '--config',
                        default='templates/generic.json')
    parser.add_argument('-u', '--urls', default='templates/input_urls.txt')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    process = CrawlerProcess(get_project_settings())
    spider = DataExtractor.Dataextractorp3ItemSpider(config=args.config, urls=args.urls)
    process.crawl(spider, config=args.config, urls=args.urls)
    process.start()
