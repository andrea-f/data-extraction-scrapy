import pytest
import os, sys
sys.path.append("../dataextractorp3/")
from spiders.exceptions import FileReadError
import scrapy
import mock
import pprint
import dataextractorp3
#### SOME TESTING CONFIGURATION
from pipelines import ExtractImagePipeline, JsonWriterPipeline
image_pipe = ExtractImagePipeline()
json_pipe  = JsonWriterPipeline()

from spiders.DataExtractor import Dataextractorp3ItemSpider
INPUT_URLS   = os.path.abspath(os.path.join(os.getcwd(), "../templates/input_urls.txt"))
INPUT_CONFIG = os.path.abspath(os.path.join(os.getcwd(), "../templates/generic.json"))
data_extractor = Dataextractorp3ItemSpider(config=INPUT_CONFIG, urls=INPUT_URLS)

#### END TESTING CONFIGURATION

@pytest.mark.parametrize(('file_name, result'), [
    ('doesntexist', Exception),
])
def test_config_file_does_not_exist(file_name, result):
    with pytest.raises(result):
        assert data_extractor.read_config_file(file_name=file_name) == result

@pytest.mark.parametrize(('file_name, result'), [
    (INPUT_CONFIG, dict),
])
def test_config_file_exists(file_name, result):
    assert isinstance(data_extractor.read_config_file(file_name=file_name), result) == True

@pytest.mark.parametrize(('file_name, result'), [
    (INPUT_URLS, list),
])
def test_read_input_urls_file(file_name, result):
    assert isinstance(data_extractor.read_config_file(file_name=file_name)['urls'], result) == True

def test_yielding_a_request_form_start_requests():
    """ Test that the start_request method yields a request object """
    res = isinstance(list(data_extractor.start_requests())[0], scrapy.http.request.Request)   
    assert res == True   

@mock.patch('scrapy.http.response')
def _mock_response(response, text=""):
    response.text = text
    response.url = "http://just.testing/"
    response.meta = {}
    return response

def test_parse_works_for_title():
    text = open('test_html_input_bbc.html', 'r').read()
    response = _mock_response(text=text)
    # Populate config object
    data_extractor.read_config_file(file_name=INPUT_URLS)
    # Get the html extracted information from the response
    resp = list(data_extractor.parse(response))[0]
    print(resp)
    assert resp['headline'] == "US Open 2016: Novak Djokovic beats Kyle Edmund in fourth round - BBC Sport"

def test_extract_images():
    text = open('test_html_input_bbc.html', 'r').read()
    response = _mock_response(text=text)
    # Populate config object
    data_extractor.read_config_file(file_name=INPUT_URLS)
    # Get the html extracted information from the response
    resp = list(data_extractor.parse(response))[0]
    images = resp['images']
    assert len(images) >= 1

@pytest.mark.parametrize(('resolutions, result'), [
    ([
        'http://ichef.bbci.co.uk/onesport/cps/240/cpsprodpb/A8FB/production/_89895234_tennis.png',
        'http://ichef.bbci.co.uk/onesport/cps/320/cpsprodpb/A8FB/production/_89895234_tennis.png',
        'http://ichef.bbci.co.uk/onesport/cps/480/cpsprodpb/A8FB/production/_89895234_tennis.png',
        'http://ichef.bbci.co.uk/onesport/cps/624/cpsprodpb/A8FB/production/_89895234_tennis.png',
        'http://ichef.bbci.co.uk/onesport/cps/800/cpsprodpb/A8FB/production/_89895234_tennis.png',
        'http://ichef.bbci.co.uk/onesport/cps/976/cpsprodpb/A8FB/production/_89895234_tennis.png'
    ], 'http://ichef.bbci.co.uk/onesport/cps/976/cpsprodpb/A8FB/production/_89895234_tennis.png'),
])
def test_get_best_resolution(resolutions, result):
    """
    Tests that the get best resolution method Works.
    """
    assert image_pipe._select_best_resolution(resolutions) == result

class mock_spider():
    """ Mocks spider class """

@pytest.mark.parametrize(('raw_html, result'), [
    ("""
        <title>Who won your club's Player of the Year award?</title>
        <figure class="widge-figure" property="associatedMedia">
        <div class="widge-figure__body" typeof="ImageObject">

                            <div class="auto-size-16/9">
                    <img class="auto-size__target postpone-load--fade-in widge-figure__image" data-src="http://e1.365dm.com/17/04/16-9/20/skysports-ngolo-kante-chelsea-pfa-award_3937274.jpg?20170423234739" alt="N'Golo Kante with the PFA Players' Player of the Year award" itemprop="image" src="http://e1.365dm.com/17/04/16-9/20/skysports-ngolo-kante-chelsea-pfa-award_3937274.jpg?20170423234739">
                    <noscript>
                        &lt;img class="widge-figure__image auto-size__target" src="http://e1.365dm.com/17/04/16-9/20/skysports-ngolo-kante-chelsea-pfa-award_3937274.jpg?20170423234739" alt="N&amp;#039;Golo Kante with the PFA Players&amp;#039; Player of the Year award" itemprop="image"&gt;
                    </noscript>
                </div>
            
        </div><div class="widge-figure__text" property="caption">
        N'Golo Kante with the PFA Players' Player of the Year award</div>
        </figure>
    """,
    {
        "url": "http://www.skysports.com/football/news/11661/10871659/who-won-your-clubs-player-of-the-year-award",
        "headline": "Who won your club's Player of the Year award?",
        "images": [{"url": "http://e1.365dm.com/17/04/16-9/20/skysports-ngolo-kante-chelsea-pfa-award_3937274.jpg?20170423234739", "caption": "N'Golo Kante with the PFA Players' Player of the Year award"}]
    }),
])
def test_item_is_extracted_from_html(raw_html, result):
    """ Should save a json object, adding to a file if the file exists via scrapy pipeline, and return item"""
    text = raw_html
    response = _mock_response(text=text)
    response.url = "http://www.skysports.com/football/news/11661/10871659/who-won-your-clubs-player-of-the-year-award"
    # Get the html extracted information from the response
    resp = list(data_extractor.parse(response))[0]
    item = image_pipe.process_item(resp, mock_spider())
    #pprint.pprint(item)
    assert item == result

@pytest.mark.parametrize(('item'),[({
        "url": "http://www.skysports.com/football/news/11661/10871659/who-won-your-clubs-player-of-the-year-award",
        "headline": "Who won your club's Player of the Year award?",
        "images": [{"url": "http://e1.365dm.com/17/04/16-9/20/skysports-ngolo-kante-chelsea-pfa-award_3937274.jpg?20170423234739", "caption": "N'Golo Kante with the PFA Players' Player of the Year award"}]
    })])
def test_json_output_is_saved(item):
    file_name = './test_generated_output.json'
    json_pipe.process_item(item, mock_spider(), file_name=file_name)
    assert os.path.isfile(file_name) == True
